# Changelog

## 0.1.1

- Version ID added to the responses of the Version API endpoints.
- Docker deployment files updated.

## 0.1.0

- Initial version released.
