FROM python:3.11-alpine
LABEL maintainer="cismat@gmail.com"

ENV HOME_DIRECTORY="/home/user" \
    PIP_DEFAULT_TIMEOUT=100 \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_NO_CACHE_DIR=off \
    PYTHONDONTWRITEBYTECODE=1 \
    PYTHONUNBUFFERED=1 \
    USERNAME="user" \
    VENV_PATH="/opt/pysetup/.venv"

ENV PATH="$VENV_PATH/bin:$PATH"

RUN apk update \
    && apk add curl g++ make libffi-dev binutils \
    && adduser -D -h "${HOME_DIRECTORY}" "${USERNAME}" "${USERNAME}" \
    && mkdir -p "${HOME_DIRECTORY}/docs" \
    && chown -R "${USERNAME}:${USERNAME}" "${HOME_DIRECTORY}"

COPY requirements.txt .
RUN python -m venv "$VENV_PATH" \
    && /opt/pysetup/.venv/bin/pip install -r requirements.txt \
    && rm requirements.txt

WORKDIR "${HOME_DIRECTORY}/docs/"
USER "${USERNAME}"
