# Docs Manager - documentation

This repository contains the documentation for the Docs Manager application.

The browsable documentation for the app is available at
https://docs-manager.readthedocs.io/.
