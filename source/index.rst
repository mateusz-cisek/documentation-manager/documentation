Welcome to Docs Manager's documentation!
========================================

Docs Manager is a simple, user-friendly app designed for hosting and managing
documentation files.

Accessing documentation, especially technical documentation, may be a bit
annoying sometimes. We sometimes keep it in the code repositories (like Git),
and reading it sometimes involves installing some libraries (eg. MkDocs or
Sphinx) and building it. It may be even more difficult for less-technical users.
With this app, you can take care of that whole process on CI/CD (or do it
manually), upload the output and everyone can access it easily.


Features
--------

The list of features is a bit short at the moment:

* Three types of documentation are supported: HTML, Markdown, and binary (which
  means anything else), it's possible to browse and download uploaded files,
* Multiple versions can be uploaded for each project, each project can be
  assigned to a group, and the groups can be nested. The documentation type can
  be different for each version.


Limitations
-----------

There are some limitations that should be considered:

* The UI is limited to browsing the data at the moment. There is Swagger docs
  available and it can be used to create and update projects, groups, versions,
* The maximum size of the uploaded files can only be changed with the proxy
  configuration files,
* There is no authentication added yet so anyone can upload and access the
  uploadeded files. Because of that, it's not recommended to install it in
  publically available locations,
* Docker images available on Docker Hub are built for two architectures (`amd64`
  and `arm64`) but it's necessary to specify the architecure in the tag, eg.
  ``mateuszcisek/docs-manager-api:0.1.0-amd64``,

and there's probably more that I forgot to mention.


Roadmap
-------

The roadmap at the moment consists of the following features:

* Adding the missing UI capabilities,
* Adding more configuration options,
* Enabling implementation of custom documentation processors,
* Adding configuration for the documentation processors,
* Adding tests for the UI application,
* Adding documentation for developers.

This list is probably going be get longer.


Contents
--------

.. toctree::
   :maxdepth: 2

   content/deployment/index.rst
   content/changelog.md

.. toctree::
   :maxdepth: 3
   :caption: User guide

   content/user-guide/index.rst

.. toctree::
   :caption: Project links

   Repositories <https://gitlab.com/mateusz-cisek/documentation-manager>
   Docker Hub - REST API <https://hub.docker.com/r/mateuszcisek/docs-manager-api>
   Docker Hub - Web UI <https://hub.docker.com/r/mateuszcisek/docs-manager-ui>
   Docker Hub - Web Proxy <https://hub.docker.com/r/mateuszcisek/docs-manager-proxy>


Screenshots
-----------

.. figure:: _static/images/list-of-projects.png
    :alt: List of projects

    List of projects

|

.. figure:: _static/images/project-actions.png
    :alt: Project actions

    Project actions

|

.. figure:: _static/images/versions-for-project.png
    :alt: List of versions for a project

    List of versions for a project
