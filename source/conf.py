project = "Docs Manager"
copyright = "2023, Mateusz Cisek"
author = "Mateusz Cisek"

release = ""

source_suffix = [".rst", ".md"]
extensions = ['myst_parser']

templates_path = ["_templates"]

html_theme = "furo"
html_static_path = ["_static"]
