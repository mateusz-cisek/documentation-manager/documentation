Kubernetes
==========

This page describes how to run and configure the application using into a
`Kubernetes <https://kubernetes.io/docs/home/>`_ cluster using `Helm
<https://helm.sh/docs/>`_.


Prerequisites
-------------

You must have access to a Kubernetes cluster and Helm installed in your system.

The application was tested with Helm 3.9.0 and cluster running Kubernetes 1.26.9
but it's highly likely that it will work with older versions of both of them.

The Helm chart is available in the `deployment Git repository
<https://gitlab.com/mateusz-cisek/documentation-manager/deployment/-/tree/release_0.1/kubernetes/docs-manager>`_
and in the `Helm repository on Gitlab
<https://gitlab.com/mateusz-cisek/documentation-manager/deployment/-/packages>`_.
To add the repository to your local Helm configuration, please run the following
command:

.. code-block:: bash

    helm repo add docs-manager https://gitlab.com/api/v4/projects/48813510/packages/helm/stable/

The commands below assume that you use the Gitlab repository added with the
command above. If you decide to use the chart from the Git repository, clone or
download it, and navigate to directory ``deployment/kubernetes/docs-manager``
before running the commands.

Also, all commands will be applied to namespace called ``docs-manager``. You can
use a different namespace if you want, just remember to adjust the value in the
commands.


Installation
------------

Here is a sample command that you can use to install the application:

.. code-block:: bash

    helm upgrade \
        --install \
        --namespace docs-manager \
        --set secretKey=some-really-long-super-secret-key-123 \
        --set publicHost=example.com \
        --set publicUrl=https://example.com \
        --set timeZone=Europe/London \
        --set database.auth.username=docs_manager \
        --set database.auth.password=docs_manager \
        --set database.auth.database=docs_manager \
        docs-manager \
        docs-manager/docs-manager

If you install the application from the local directory, replace the last part
of the commands above (``docs-manager/docs-manager``) with a dot character
(``.``) to indicate that you want to use the chart from the current working
directory, and run the following command first to download required
dependencies:

.. code-block:: bash

    helm dependency update

Simple validation rules are defined in the chart. The values listed above are
required and the deployment will fail if any on them is missing.

To uninstall the application, use this command:

.. code-block:: bash

    helm uninstall -n docs-manager docs-manager


Configuration
-------------

The documentation of all values that can be adjusted is available `here
<https://gitlab.com/mateusz-cisek/documentation-manager/deployment/-/blob/release_0.1/kubernetes/docs-manager/README.md>`_.
