Deployment
==========

By default, the app can be deployed using Docker Compose or Kubernetes.

To learn more, please see the dedicated section for those deployment methods:

.. toctree::
   :maxdepth: 2

   docker-compose.rst
   kubernetes.rst

