Docker Compose
==============

This page describes how to run and configure the application using `Docker
Compose <https://docs.docker.com/compose/>`_.


Prerequisites
-------------

You must have Docker and Docker Compose installed to run the application.
Internet access is required for the required images to be downloaded.

Please note that the commands below will work for Docker Compose V2. For V1, use
``docker-compose`` instead of ``docker compose``. To learn more, please see the
`migration guide <https://docs.docker.com/compose/migrate/>`_.


Starting the application
------------------------

To start the application, run the following command:

.. code-block:: bash

    docker compose up -d

And this command can be used to stop it:

.. code-block:: bash

    docker compose down


The content of the deployment files is described below.


Deployment files
----------------

To start the application, two files are required:

* ``docker-compose.yaml`` - with the definition of all services,
* ``.env`` - with all required environment variabled.

The sample content for both those files are stored in the `repository
<https://gitlab.com/mateusz-cisek/documentation-manager/deployment/-/tree/release_0.1/docker>`_
and can be found below.

.. code-block:: yaml
    :caption: docker-compose.yaml 

    version: '3'

    services:
      database:
        container_name: docs-manager-database
        image: ${DATABASE_IMAGE_NAME}:${DATABASE_IMAGE_TAG}
        restart: unless-stopped
        env_file: .env
        volumes:
          - database_storage:/var/lib/postgresql/data
        healthcheck:
          test: pg_isready -U "${POSTGRES_USER}" -d "${POSTGRES_DB}"
          interval: 10s
          timeout: 10s
          retries: 3

      web-api:
        container_name: docs-manager-api
        image: ${WEB_API_IMAGE_NAME}:${WEB_API_IMAGE_TAG}
        restart: unless-stopped
        command:
          - /bin/sh
          - -c
          - |
            python manage.py collectstatic --noinput
            python manage.py migrate
            gunicorn --bind 0.0.0.0:8000 --threads 4 docs_manager.config.wsgi:application
        env_file: .env
        environment:
          PROXY_URL: ${PUBLIC_URL}
        depends_on:
          database:
            condition: service_healthy
        volumes:
          - data_storage:/home/user/data
          - static_files_storage:/home/user/static
        healthcheck:
          test: curl -f http://localhost:8000/api/v1/system/healthcheck/ || exit 1
          interval: 10s
          timeout: 10s
          retries: 3
          start_period: 30s

      web-ui:
        container_name: docs-manager-ui
        image: ${WEB_UI_IMAGE_NAME}:${WEB_UI_IMAGE_TAG}
        restart: unless-stopped
        env_file: .env
        healthcheck:
          test: curl -f http://localhost:3000/env.js || exit 1
          interval: 10s
          timeout: 10s
          retries: 3
          start_period: 30s

      web-proxy:
        container_name: docs-manager-proxy
        image: ${WEB_PROXY_IMAGE_NAME}:${WEB_PROXY_IMAGE_TAG}
        restart: unless-stopped
        env_file: .env
        depends_on:
          web-api:
            condition: service_healthy
          web-ui:
            condition: service_healthy
        ports:
          - ${PUBLIC_PORT:?Environment variable PUBLIC_PORT is not set.}:8080
        volumes:
          - static_files_storage:/app/static
          - data_storage:/app/data
        healthcheck:
          test: curl -f http://localhost:8080/api/v1/system/healthcheck/ || exit 1
          interval: 10s
          timeout: 10s
          retries: 3
          start_period: 30s

    volumes:
      data_storage:
      database_storage:
      static_files_storage:

.. code-block:: bash
    :caption: .env

    ########################################
    # Docker images to use
    ########################################

    DATABASE_IMAGE_NAME="postgres"
    DATABASE_IMAGE_TAG="15.2"
    WEB_API_IMAGE_NAME="mateuszcisek/docs-manager-api"
    WEB_API_IMAGE_TAG="0.1.0-amd64"
    WEB_UI_IMAGE_NAME="mateuszcisek/docs-manager-ui"
    WEB_UI_IMAGE_TAG="0.1.0-amd64"
    WEB_PROXY_IMAGE_NAME="mateuszcisek/docs-manager-proxy"
    WEB_PROXY_IMAGE_TAG="0.1.0-amd64"

    ########################################
    # Web API settings
    ########################################

    DATABASE_HOST="database"
    DATABASE_NAME="docs_manager"
    DATABASE_PASSWORD="docs_manager"
    DATABASE_PORT="5432"
    DATABASE_USER="docs_manager"
    DEBUG="False"
    LOGGING_LEVEL="info"
    PUBLIC_HOST="localhost"
    PUBLIC_PORT="8080"
    PUBLIC_URL="http://localhost:8080"
    SECRET_KEY="super-secret-django-key-1234567890"
    TIME_ZONE="Europe/London"

    ########################################
    # Database settings
    ########################################

    POSTGRES_DB="${DATABASE_NAME:?Environment variable DATABASE_NAME is not set.}"
    POSTGRES_PASSWORD="${DATABASE_PASSWORD:?Environment variable DATABASE_PASSWORD is not set.}"
    POSTGRES_USER="${DATABASE_USER:?Environment variable DATABASE_USER is not set.}"

The values used in the database settings must be the same as the ones used in
the web API settings.

The only database engine that is officially supported is PostreSQL so make sure
the ``DATABASE_IMAGE_*`` environment variables point to a PostgreSQL database
image.


Persistence and backups
-----------------------

As you may have noticed, there are three volumes that are used in the
configuration above. The volume with the static files - ``static_files_storage``
- is not that important as the static files will be regenerated automatically
when the application starts. It's necessary because the static files are
generated by the web API application but must also be accessed by the proxy
container.

The other two volumes - ``data_storage`` and ``database_storage`` - are more
important. They store the uploaded files and the database data. At the moment,
there is no automated way to create and restore backups so that must be done
manually.


Environment variables
---------------------

This section describes the environment variables used when deploying the app
with Docker. In the example above, all of them are defined in the ``.env`` file.

Please note that some environment variables used in the example file above are
not listed below and don't have to be defiend in the ``.env`` file. Those
internal values are used in the application but don't need to be changed.


Docker images to use
^^^^^^^^^^^^^^^^^^^^

+--------------------------+-----------------------------------------------------+----------------------------------------+
| Variable name            | Description                                         | Sample value                           |
+==========================+=====================================================+========================================+
| ``DATABASE_IMAGE_NAME``  | The name of the database image that should be used. | ``postgres``                           |
+--------------------------+-----------------------------------------------------+----------------------------------------+
| ``DATABASE_IMAGE_TAG``   | The tag of the database image that should be used.  | ``15.2``                               |
+--------------------------+-----------------------------------------------------+----------------------------------------+
| ``WEB_API_IMAGE_NAME``   | The name of the web API image that should be used.  | ``mateuszcisek/docs-manager-api``      |
+--------------------------+-----------------------------------------------------+----------------------------------------+
| ``WEB_API_IMAGE_TAG``    | The tag of the web API image that should be used.   | ``0.1.0``                              |
+--------------------------+-----------------------------------------------------+----------------------------------------+
| ``WEB_PROXY_IMAGE_NAME`` | The name of the proxy image that should be used.    | ``mateuszcisek/docs-manager-proxy``    |
+--------------------------+-----------------------------------------------------+----------------------------------------+
| ``WEB_PROXY_IMAGE_TAG``  | The tag of the proxy image that should be used.     | ``0.1.0``                              |
+--------------------------+-----------------------------------------------------+----------------------------------------+
| ``WEB_UI_IMAGE_NAME``    | The name of the web UI image that should be used.   | ``mateuszcisek/docs-manager-ui``       |
+--------------------------+-----------------------------------------------------+----------------------------------------+
| ``WEB_UI_IMAGE_TAG``     | The tag of the web UI image that should be used.    | ``0.1.0``                              |
+--------------------------+-----------------------------------------------------+----------------------------------------+


Backend application settings
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

+-----------------------+--------------------------------------------------------------------------+----------------------------------------+
| Variable name         | Description                                                              | Sample value                           |
+=======================+==========================================================================+========================================+
| ``DATABASE_HOST``     | The host of the to database to connect to.                               | ``database``                           |
+-----------------------+--------------------------------------------------------------------------+----------------------------------------+
| ``DATABASE_NAME``     | The name of the to database user to use.                                 | ``docs_manager``                       |
|                       | It must be the same as the value of ``POSTGRES_DB``.                     |                                        |
+-----------------------+--------------------------------------------------------------------------+----------------------------------------+
| ``DATABASE_PASSWORD`` | The password of the to database user to use.                             | ``docs_manager``                       |
|                       | It must be the same as the value of ``POSTGRES_PASSWORD``.               |                                        |
+-----------------------+--------------------------------------------------------------------------+----------------------------------------+
| ``DATABASE_PORT``     | The port of the to database to use.                                      | ``5432``                               |
+-----------------------+--------------------------------------------------------------------------+----------------------------------------+
| ``DATABASE_USER``     | The user of the to database to use.                                      | ``docs_manager``                       |
|                       | It must be the same as the value of ``POSTGRES_USER``.                   |                                        |
+-----------------------+--------------------------------------------------------------------------+----------------------------------------+
| ``LOGGING_LEVEL``     | The logging level to set.                                                | ``info``                               |
+-----------------------+--------------------------------------------------------------------------+----------------------------------------+
| ``PUBLIC_HOST``       | The public host at which the application will be available at.           | ``example.com``                        |
+-----------------------+--------------------------------------------------------------------------+----------------------------------------+
| ``PUBLIC_PORT``       | The public port at which the application will be available at.           | ``8080``                               |
+-----------------------+--------------------------------------------------------------------------+----------------------------------------+
| ``PUBLIC_URL``        | The public URL at which the application will be available at.            | ``https://example.com:8080``           |
|                       | It must be the same as ``PUBLIC_HOST`` but with added protocol and port. |                                        |
+-----------------------+--------------------------------------------------------------------------+----------------------------------------+
| ``SECRET_KEY``        | The secret key used to provide cryptographic stuff,                      | ``super-secret-django-key-1234567890`` |
|                       | make it long and unpredictable.                                          |                                        |
+-----------------------+--------------------------------------------------------------------------+----------------------------------------+
| ``TIME_ZONE``         | The application timezone.                                                | ``Europe/London``                      |
+-----------------------+--------------------------------------------------------------------------+----------------------------------------+


Database settings
^^^^^^^^^^^^^^^^^

+-----------------------+------------------------------------------------------------+------------------+
| Variable name         | Description                                                | Sample value     |
+=======================+============================================================+==================+
| ``POSTGRES_DB``       | The name of the to database user to use.                   | ``docs_manager`` |
|                       | It must be the same as the value of ``DATABASE_NAME``.     |                  |
+-----------------------+------------------------------------------------------------+------------------+
| ``POSTGRES_PASSWORD`` | The password of the to database user to use.               | ``docs_manager`` |
|                       | It must be the same as the value of ``DATABASE_PASSWORD``. |                  |
+-----------------------+------------------------------------------------------------+------------------+
| ``POSTGRES_USER``     | The user of the to database to use.                        | ``docs_manager`` |
|                       | It must be the same as the value of ``DATABASE_USER``.     |                  |
+-----------------------+------------------------------------------------------------+------------------+

