Documentation types
===================

The application supports three types of documentation at the moment: HTML,
Markdown, and binary.


HTML
----

HTML documentation consists of at least one file called ``index.html`` that must
be uploaded as a ZIP archive with that file in its root directory. Other HTML
files can be included in the archive and linked between each other. Static files
(like CSS, JavaScript, or images) are supported and will be rendered correctly
in the browser.

This type can be used to host documentation created by tools such as Sphinx and
Mkdocs as well as custom HTML documents.


Markdown
--------

Markdown documentation consists of at least one file called ``index.md`` that
must be uploaded as a ZIP archive with that file in its root directory. Other
Markdown files can be included in the archive and linked between each other. The
documents will be parsed and displayed as HTML files. Static files (like images)
are supported and will be rendered correctly in the browser.

Markdown documentation type can be used for hosting simple documentation such as
README files, changelogs, or a bit more complex documents that consists of
multiple pages.


Binary
------

Binary documentation can be used to host anything. There are no limitation when
it comes to the type or size of the uploaded files. The documents will be
rendered by the browser when it's supported (eg. PDF files will be usually
rendered). Otherwise, it will be possible to download the file.

This type can be used to host any files, especially when they are not supported
by other documentation types, such as PDF, Microsoft Word files, plain text,
etc.
