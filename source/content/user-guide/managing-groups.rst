Managing groups
=================

The user interface is limited to browsing data. Creating, updating, and removing
groups is only possible using provided REST API. To access interactive Swagger
docs for interacting with the endpoints, visit the ``/api`` path for your
installation. For example, if the application is available at
`https://example.com/ <https://example.com/>`_ then the URL of the Swagger docs
is `https://example.com/api/ <https://example.com/api/>`_.


Adding a new group
--------------------

Endpoint: ``/api/v1/groups/``

Request method: ``POST``

Request body:

.. code-block:: json

    {
        "name": "Sample group",
        "parent": 1,
        "slug": "sample-group"
    }

Fields:

* ``name`` (*string*): The name of the group,
* ``parent`` (*integer*): The ID of the parent group or ``null``,
* ``slug`` (*string*): The slug of the group, used in URLs.

Response status codes:

* ``201`` if the group has been created,
* ``400`` if the request body is incorrect.

Response body:

.. code-block:: json

    {
        "full_name": "Sample group",
        "id": 5,
        "name": "Sample group"
    }

Fields:

* ``full_name`` (*string*): The full name of the group, including the full name
  of the parent's group.
* ``id`` (*int*): The ID of the group,
* ``name`` (*string*): The name of the group.


Updating an existing group
--------------------------

Endpoint: ``/api/v1/groups/{group-id}/``

Request methods:

* update: ``PUT``,
* partial update: ``PATCH``.

Request body:

.. code-block:: json

    {
        "name": "Sample group",
        "parent": 1,
        "slug": "sample-group"
    }

Fields:

* ``name`` (*string*): The name of the group,
* ``parent`` (*integer*): The ID of the parent group or ``null``,
* ``slug`` (*string*): The slug of the group, used in URLs.

Response status codes:

* ``200`` if the group has been updated,
* ``400`` if the request body is incorrect.

Response body:

.. code-block:: json

    {
        "full_name": "Sample group",
        "id": 5,
        "name": "Sample group"
    }

Fields:

* ``full_name`` (*string*): The full name of the group, including the full name
  of the parent's group.
* ``id`` (*int*): The ID of the group,
* ``name`` (*string*): The name of the group.


Removing a group
----------------

Endpoint: ``/api/v1/groups/{group-id}/``

Request method: ``DELETE``

Request body should be empty.

Response status codes:

* ``204`` if the group has been deleted,
* ``400`` if the something went wrong.

Response body is empty.
