Useful terms
============

There are a few terms that are used in the application and across the
documentation, and it's necessary to understand them to use the app efficiently.


Project
-------

Project represents a single piece of software or other resource that can be
documented. It can be a whole application or just a small piece of it, eg. a
library, a module, etc. You can also use it to describe non-software resources
such as user or developer documentation, quick start guides etc.


Version
-------

For each project, we can create as many versions as necessary. It may be a good
idea to add a new version for a project when the related sofware is released to
keep it consistent.


Group
-----

Groups can be used to create a logical structure in your documentation
repository. Each group can optionally have a parent group, and can have as many
projects as necessary.


Examples
--------

Given a software project that consists of a mobile application, a web
application, and REST API, with separate technical documentation for each of
those (such as Markdown files, Mkdocs, or Sphinx), we can create a group called
"My Project", and then create three projects inside of that group, one for each
application.

If we write documentation that applies to the whole project (like a MS Word
document that we convert to PDF format), we can add a new project for hosting
that specific document.
