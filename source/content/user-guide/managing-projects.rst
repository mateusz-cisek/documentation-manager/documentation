Managing projects
=================

The user interface is limited to browsing data. Creating, updating, and removing
projects is only possible using provided REST API. To access interactive Swagger
docs for interacting with the endpoints, visit the ``/api`` path for your
installation. For example, if the application is available at
`https://example.com/ <https://example.com/>`_ then the URL of the Swagger docs
is `https://example.com/api/ <https://example.com/api/>`_.


Adding a new project
--------------------

Endpoint: ``/api/v1/projects/``

Request method: ``POST``

Request body:

.. code-block:: json

    {
        "group": 1,
        "name": "Sample project",
        "slug": "sample-project"
    }

Fields:

* ``group`` (*integer*): The ID of the project group or ``null``,
* ``name`` (*string*): The name of the project,
* ``slug`` (*string*): The slug of the project, used in URLs.

Response status codes:

* ``201`` if the project has been created,
* ``400`` if the request body is incorrect.

Response body:

.. code-block:: json

    {
        "breadcrumbs": [],
        "default_version": null,
        "id": 5,
        "name": "string"
    }

Fields:

* ``breadcrumbs`` (*int[]*): List of group IDs that are parent to the project,
* ``default_version`` (*object*): ``null`` because it's a new project without
  any assigned versions,
* ``id`` (*int*): The ID of the project,
* ``name`` (*string*): The name of the project.


Updating an existing project
----------------------------

Endpoint: ``/api/v1/projects/{project-id}/``

Request methods:

* update: ``PUT``,
* partial update: ``PATCH``.

Request body:

.. code-block:: json

    {
        "group": 1,
        "name": "Sample project",
        "slug": "sample-project"
    }

Fields:

* ``group`` (*integer*): The ID of the project group or ``null``,
* ``name`` (*string*): The name of the project,
* ``slug`` (*string*): The slug of the project, used in URLs.

Response status codes:

* ``200`` if the project has been updated,
* ``400`` if the request body is incorrect.

Response body:

.. code-block:: json

    {
        "breadcrumbs": [],
        "default_version": {
            "browse_url": "https://example.com/browse/sample-project::1.0.0/",
            "created_at": "2023-10-13T03:12:47.622393+01:00",
            "documentation_type": "markdown",
            "download_url": "https://example.com/download/sample-project::1.0.0/",
            "value": "3.5.5"
        },
        "id": 5,
        "name": "string"
    }

Fields:

* ``breadcrumbs`` (*int[]*): List of group IDs that are parent to the project,
* ``default_version`` (*object*): The version marked as the default one or ``null``,

  * ``browse_url`` (*string*): The URL for browsing the documentation files,
  * ``created_at`` (*datetime*): The version creation date and time,
  * ``documentation_type`` (*string*): The type of the documentation,
  * ``download_url`` (*string*): The URL for downloading the documentation files,
  * ``value`` (*string*): The version value,

* ``id`` (*int*): The ID of the project,
* ``name`` (*string*): The name of the project.


Removing a project
------------------

Endpoint: ``/api/v1/projects/{project-id}/``

Request method: ``DELETE``

Request body should be empty.

Response status codes:

* ``204`` if the project has been deleted,
* ``400`` if the something went wrong.

Response body is empty.
