Managing versions
=================

The user interface is limited to browsing data. Creating and removing versions
is only possible using provided REST API. To access interactive Swagger docs for
interacting with the endpoints, visit the ``/api`` path for your installation.
For example, if the application is available at `https://example.com/
<https://example.com/>`_ then the URL of the Swagger docs is
`https://example.com/api/ <https://example.com/api/>`_.


Adding a new version
--------------------

Endpoint: ``/api/v1/versions/``

Request method: ``POST``

Request content type: ``multipart/form-data``

Request fields:

* ``content`` (*file*): The content of the project's documentation version,
* ``documentation_type`` (*string*): The type of the version's documentation,
  available values are ``binary``, ``html``, and ``markdown`` (see the
  :doc:`Documentation types <./documentation-types>` section to learn more), 
* ``is_default`` (*boolean*): Whether or not the created version should be
  considered to be the default one; if it's the first version created for the
  project then it's going to be marked as default anyway,
* ``project`` (*integer*): The ID of the project to assign the version to,
* ``value`` (*string*): The string representation of the version; it may be a
  semver value (eg. ``1.0.0``) or something different.

Response status codes:

* ``201`` if the project has been created,
* ``400`` if the request body is incorrect.

Response content type: ``application/json``

Response body:

.. code-block:: json

    {
        "browse_url": "http://example.com/browse/project-1::1.0.0/",
        "created_at": "2023-11-09T22:54:16.081832Z",
        "documentation_type": "html",
        "download_url": "http://example.com/download/project-1::1.0.0/",
        "value": "1.0.0"
    }

Fields:

* ``browse_url`` (*string*): The URL that can be used to browse the
  documentation,
* ``created_at`` (*datetime*): The date and time of when the version was
  created,
* ``documentation_type`` (*string*): The type of the version's documentation,
* ``download_url`` (*string*): The URL that can be used to download the
  documentation,
* ``id`` (*integer*): The ID of the created version,
* ``value`` (*string*): The string representation of the version.


Removing a version
------------------

Endpoint: ``/api/v1/versions/{version-id}/``

Request method: ``DELETE``

Request body should be empty.

Response status codes:

* ``204`` if the project has been deleted,
* ``400`` if the something went wrong.

Response body is empty.
