User guide
==========

The application provides functionality that is necessary for storing and
accessing documentation files, such as creating groups, projects, and uploading
documentation for the projects. This section explains how to do all of that.

.. toctree::
   :maxdepth: 2

   useful-terms.rst
   documentation-types.rst
   managing-projects.rst
   managing-groups.rst
   managing-versions.rst
